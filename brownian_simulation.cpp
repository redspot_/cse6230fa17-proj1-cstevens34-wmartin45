#include <stdlib.h>
#include <stdio.h>
#include <unistd.h> // access
#include <math.h>
#include <assert.h>
#include "brownian_simulation.h"


typedef std::vector<Particle> ParticleArray;

const int intervals = 1000;
int brownian_simulation(std::vector<Particles>& particles int size, Paricle *particles, double box_width, std::vector<Particle>& dist2_p, int **pairs_p)
{
  double f = sqrt(2.*DELTAT);
  /* 2 is twice the radius, $2 r_c$ in Prof. Chow's Lecture 5 */
  int     boxdim = box_width / 2;
  /* Must be at least the square of twice the radius */
  double  cutoff2 = 4.;
  int     maxnumpairs = *maxnumpairs_p;
  double *dist2 = *dist2_p;
  int    *pairs = *pairs_p;

    for (int step=0; step<intervals; step++)
    {
        int retval;
        int numpairs = 0;
        while (1) 
        {
        retval = interactions(npos, pos, L, boxdim, cutoff2, dist2, pairs, maxnumpairs, &numpairs);
        if (!retval) break;
        if (retval == -1) 
        {
            dist

        free(pairs);
        free(dist2);
        maxnumpairs *= 2;
        dist2 = (double *) malloc(maxnumpairs*sizeof(double));
        pairs = (int *) malloc(2*maxnumpairs*sizeof(int));
        assert(dist2);
        } else {
        return retval;
        }
        }
        for (int p = 0; p < numpairs; p++) {
      const double krepul = 100.;
      /* TODO: Put the force calculation here using the same formula as exercise 04*/
      /* TODO: Take out this print statement */
#if 1
      if (!step) {
        printf("Particle pair %d: (%d, %d) are %g apart\n",p,pairs[2*p],pairs[2*p+1],sqrt(dist2[p]));
      }
#endif
    }
    // update positions with Brownian displacements
    for (int i=0; i<3*npos; i++)
    {
      double noise = cse6230nrand(nrand);

      pos[i] += f*noise;
    }
  }
  *maxnumpairs_p = maxnumpairs;
  *dist2_p = dist2;
  *pairs_p = pairs;

  return 0;
}

// contains all the particles for this cell which is a 
class Cell
{
    std::vector<Particle> _particles
    Point _origin;
    Point _dimenensions;

    bool is_inside(Particle p)
    {
        return 
            (p.x >= _origin.x && p.x < _origin.x + _dimensions.x) &&
            (p.y >= _origin.y && p.y < _origin.y + _dimensions.y) && 
            (p.z >= _origin.z && p.z < _origin.z + _dimensions.z);
    }
};

class Box
{
    Point _dimensions;
    Point _celldim;
    int _cols; // = x 
    int _rows; // = y 
    int _levels; // = z 

    Box(Point dimensions, float particle_radius)
    {
        const float R = sqrt(4 * particle_radius * particle_radius);

        _celldim.x = _celldim.y = _celldim.z = R;
        _cols = ceil(dimensions.x / R);
        _rows = ceil(dimensions.y / R);
        _levels = ceil(dimensions.z / R);

        if ( _cols * R > dimensions.x)
        {
            _cols--;
            _celldim.x = dimensions.x/(_cols-1);
        }
        if ( _rows * R > dimensions.y)
        {
            _rows--;
            _celldim.y = dimensions.y/(_rows-1);
        }
        if ( _levels * R > dimensions.z)
        {
            _levels--;
            _celldim.z = dimensions.z/(_levels-1);
        }

        _cells.reserve(_cols * _rows * _levels);
        for (int z = 0; z < _levels; z++)
            for (int y = 0; y < _rows; y++)
                for (int x = 0; x < _cols; x++)
                    _cells.push_back(Cell(x * _celldim.x, y * _celldim.y, z * _celldim.z));
    }


    void add_particle(Particle p)
    {
        int x = p.x/_celldim.x;
        int y = p.y/_celldim.y;
        int z = p.z/_celldim.z;

        *this(x,y,z).add(p);
    }

    Cell& operator ()(int col, int row, int level)
    {
        return _cells[(level * _rows) + (row * _cols) + col];
    }

    std::vector<Cell> _cells;
};

void brownian_motion(ParticleArray& particles, int64_t intervals)
{
    const double dt = 1e-4;
    std::uniform_real_distribution<double> rng(-1.0,1.0);
    std::chrono::microseconds run_time;

    Point *initial = new Point[size];
    Point *forces = new Point[size];

// #pragma omp parallel
    {
        double avg_dist;
        omp_set_num_threads(threads);

#pragma omp parallel for schedule(static)
        for (int i = 0; i < size; i++)
        {
            particles[i].x = abs(rng(*generator));
            particles[i].y = abs(rng(*generator));
            particles[i].z = abs(rng(*generator));

            initial[i] = particles[i];
        }

        std::chrono::high_resolution_clock::time_point begin = 
            std::chrono::high_resolution_clock::now();

        compute_forces(size, particles, forces);
        for (int t = 0;  t < time; t++)
        {
#pragma omp parallel for schedule(static)
            for (int i = 0; i < size; i++)
            {
                Point displacement;
                displacement.x = rng(*generator);
                displacement.y = rng(*generator);
                displacement.z = rng(*generator);

                particles[i].x += remainder(displacement.x * sqrt(2 * dt), 1);
                particles[i].y += remainder(displacement.x * sqrt(2 * dt), 1);
                particles[i].z += remainder(displacement.x * sqrt(2 * dt), 1);
            }

            if ((t+1) % 500 == 0)
            {
                avg_dist = 0;
#pragma omp parallel for reduction(+:avg_dist)
                for (int i = 0; i < size; i++)
                {
                    avg_dist += initial[i].distance(particles[i]);
                }
                avg_dist /= size;
                printf("time: %5d: average distance = %f\n", t+1, avg_dist);
            }
        }
        std::chrono::high_resolution_clock::time_point finish = 
            std::chrono::high_resolution_clock::now();

        run_time = std::chrono::duration_cast< std::chrono::microseconds > (finish - begin);
        std::cout << "time: " << run_time.count() << std::endl;
    }

    delete[] particles;
    delete[] initial;
}
