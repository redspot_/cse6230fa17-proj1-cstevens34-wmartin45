#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <omp.h>
#include "brownian.h"

#define INTERVAL_LEN 1000
#define DELTAT       1e-4


int debug = 0;

int64_t now()
{
    struct timespec tp;
    clock_gettime(CLOCK_MONOTONIC, &tp);
    int64_t time = tp.tv_sec * 1000000;
    time += tp.tv_nsec/1000;

    return time;
}

int main(int argc, char* argv[])
{
	int size = 10000;
	int intervals = 5;
    int threads = 1;
    int r = 0;
    int seed = 23;
    double box_width, start_time;

    FILE* input = 0;
    FILE* output = 0;

    int pos = 0;

	for (int i = 1; i < argc; i++)
	{
		if (argv[i][0] == '-')
		{
			switch(argv[i][1])
			{
				case 'd': debug = atoi(argv[++i]); break;
				case 'i': intervals = atoi(argv[++i]); break;
                case 'f': // in file
                          input = fopen(argv[++i], "r");
                          break;
                case 'n': size = atoi(argv[++i]); break;
                case 'o': // out file
                          output = fopen(argv[++i], "w");
                          break;
                case 'r': r = 1;
                          seed = atoi(argv[++i]); 
                          break;
                case 't' : threads = atoi(argv[++i]); break;
				default:
                    printf("unrecognized option %s\n", argv[i]);
			}
		}
		else printf("unrecognized option %s\n", argv[i]);
	}

    if (!input || !output)
    {
        printf("Usage: %s -n <size> -t <threads> -i <intervals> -r <seed>\n", argv[0]);
        printf("\t-f: set the input file\n");
        printf("\t-o: set the output file\n");
        printf("\t-i: number of intervals (time) to run the program\n");
        printf("\t-n: size of the simulation\n");
        printf("\t-r: set the random seed (if used) for the program\n");
        printf("\t-t: number of threads to run in parallel\n");
        printf("\n");
        exit(0);
    }

    omp_set_num_threads(threads);

    int type;
    // read the input file;
    fscanf(input, "%d %lf %lf", &size, &start_time, &box_width);
    while (fgetc(input) != '\n'); 

    Box box;
    create_box(&box, box_width, size, 0);
    // box.init(size, seed);

    for (int i = 0; i < size; i++)
    {
        Particle *p = &box.particles.values[box.particles.size++];
        fscanf(input, "%d %lf %lf %lf", &type, &p->position.x, &p->position.y, &p->position.z);
    }

    printf( "Number of particles: %d\n", size);
    printf( "Number of intervals to simulate: %d\n", intervals);
    printf( "Using random seed: %d\n", seed);
    printf( "Using thread count: %d\n", threads);

    // initialize random number
    const double volume_constant = (4.0*PI)/3.0;
    double volume_fraction = (volume_constant * size) / (box_width*box_width*box_width);

    // seems wrong but not sure what this is used for--interaction volume is constant regardless of size and interactions per particle is just 8 times diffusion-volume.  pretty sure physics doesnt work like this.
    double interaction_volume = 8 * volume_constant;
    double interactions_per_particle = (size * interaction_volume) / (box_width*box_width*box_width);

    printf("With %d particles of radius 1 and a box width of %f, the volume fraction is %g.\n",size,box_width,volume_fraction);
    printf("The interaction volume is %g, so we expect %g interactions per particle, %g overall.\n",interaction_volume,interactions_per_particle,interactions_per_particle * size / 2.);
    // printf("Allocating space for %ld interactions\n",n);

    int64_t before_sim_timestamp = now();
    for (int i = 0; i < intervals; i++)
    {
        int64_t begin = now();

        run(&box, 1000);
        
        int64_t finish = now();
        int64_t run_time = finish - begin;
        int64_t cum_time = finish - before_sim_timestamp;
        printf( "time: %lf, %lf\n", run_time/1000000.0, cum_time/1000000.0);
        if (i % 5 == 0)
          printf( "Done interval: %d\n", i);

        //write frame
        fprintf(output, "%d\n", size);
        fprintf(output, "%f %f\n", start_time+i*INTERVAL_LEN*DELTAT, box_width);
        for (int i = 0; i < box.particles.size; i++)
        {
            Particle *p = &box.particles.values[i];
            fprintf(output, "0 %f %f %f\n", p->position.x, p->position.y, p->position.z);
        }
    }
    int64_t after_sim_timestamp = now();
    int64_t microseconds = after_sim_timestamp - before_sim_timestamp;
    double seconds = microseconds/1000000.0;
    printf( "Time: %lf for %d intervals\n", seconds, intervals);
}
