#include <stdlib.h>
#include <stdio.h>
#include <unistd.h> // access
#include <math.h>
#include <assert.h>
#include "bd.h"

cse6230nrand_t *rng = NULL;

void init_rng(int n, int seed)
{
    rng = malloc(sizeof(cse6230nrand_t) * n);
    for (int i = 0; i < n; i ++)
    {
        cse6230nrand_seed(seed + i, &rng[i]);
    }
}

void move_particles(int numpairs, int* pairs, double* pos, double* dist2) {
  for (int p = 0; p < numpairs; p++) {
    int p1 = pairs[p*2 + 0] * 3;
    int p2 = pairs[p*2 + 1] * 3;

    pos[p1 + 0] += dist2[p*3+0];
    pos[p1 + 1] += dist2[p*3+1];
    pos[p1 + 2] += dist2[p*3+2];

    pos[p2 + 0] -= dist2[p*3+0];
    pos[p2 + 1] -= dist2[p*3+1];
    pos[p2 + 2] -= dist2[p*3+2];
  }
}

int bd(int npos, double *pos, double L, const int *types, int *maxnumpairs_p, double **dist2_p, int **pairs_p)
{
  const double F = sqrt(2.*DELTAT);
  /* 2 is twice the radius, $2 r_c$ in Prof. Chow's Lecture 5 */
  // i think this incorrectly truncates the box width (L) to be the floor(L/2) * 2, which for our sim means that the box-width of 59.3 becomes 58.
  // realistically i think this should be the ceil(L/2)
  int     boxdim = L / 2;
  /* Must be at least the square of twice the radius */
  double  cutoff2 = 4.;
  int     maxnumpairs = *maxnumpairs_p;
  double *dist2 = *dist2_p;
  int    *pairs = *pairs_p;

  //printf("interval\n");
  for (int step=0; step<INTERVAL_LEN; step++)
  {
    int retval;
    int numpairs = 0;
    int threads = omp_get_max_threads();

    while (1) {
      retval = interactions(npos, pos, L, boxdim, cutoff2, dist2, pairs, maxnumpairs, &numpairs);
      if (!retval) break;
      if (retval == -1) {
        free(pairs);
        free(dist2);
        maxnumpairs *= 2;
        dist2 = (double *) malloc(3*(maxnumpairs+threads)*sizeof(double));
        pairs = (int *) malloc(2*(maxnumpairs+threads)*sizeof(int));
        assert(dist2);
      } else {
        return retval;
      }
    }

    move_particles(numpairs, pairs, pos, dist2);

    // update positions with Brownian displacements
#pragma omp parallel
    {
      int thread = omp_get_thread_num();
#pragma omp for
      for (int i=0; i<3*npos; i++)
      {
        double noise = cse6230nrand(rng + thread);

        pos[i] += F*noise;
      }
    }
  }
  *maxnumpairs_p = maxnumpairs;
  *dist2_p = dist2;
  *pairs_p = pairs;

  return 0;
}
