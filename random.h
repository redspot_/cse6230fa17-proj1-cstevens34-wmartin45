#if !defined(RANDOM_H)
#define RANDOM_H

#include <cmath>
// source taken from Intel with permission
// https://software.intel.com/en-us/articles/fast-random-number-generator-on-the-intel-pentiumr-4-processor/

class UniformDistribution
{
private:
    double _min, _max;

public:
    inline UniformDistribution(double min = 0, double max = 1) : _min(min), _max(max) {}

    template <typename T> double operator()(T& rng)
    {
        return static_cast<double>(rng()) / rng.max * (_max - _min) + _min;
    }
};

class NormalDistribution
{
private:
    double _mean;
    double _deviation;

public:
    inline NormalDistribution(double mean, double deviation) : _mean(mean), _deviation(deviation) {}
    const double PI = 3.1415926535897932;

    template <typename T> double operator()(T& rng)
    {
        UniformDistribution u(1E-9,.999999999);
	double u1 = u(rng);
	double u2 = u(rng);


        double theta = 2 * PI * u1;
        double rho = sqrt(-2 * log(1 - u2));
	// printf ("random: %f, %f --> %f\n", u1, u2, rho);
        double scale = _deviation  * rho;
        return _mean + scale * cos(theta);
        // double y = _mean + scale * sin(theta);
    }
};

class Random
{
private:
    int _bits;

public:
    static const int max = 0x7FFF;

    Random(double seed = 0) : _bits(seed) {}

    inline int operator()()
    { 
          /* This is cheap linear congruential generator that should never be
           * used for a scientific application where we need to trust the
           * validity of the statistics that we are generating.  I feel like I
           * devoted plenty of time in class to this, *and* you were using the
           * much better Mersenne Twister PRNG in an earlier version of the
           * code. */

        /*
         * https://software.intel.com/en-us/articles/fast-random-number-generator-on-the-intel-pentiumr-4-processor
         * the validity was published by intel and seems to pass uniformity tests.  I am not sure whether 
         * it passes a chi-square test or not.  I did not have any references about statistical tests for 
         * the cse6230 randome number generator hence my skepticism--the only mention in class was that it 
         * performs well for parallel programming.
         *
         * I am not defending this choice because I honestly agree with your comments above, it was chosen 
         * rather to see if it had any effect on the performance since it was so cheap to implement.
         */
          _bits = (214013*_bits+2531011); 
          return (_bits>>16)&0x7FFF; 
    } 
};

#endif
